/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_display_list.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 12:56:51 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/03 21:09:51 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void			print_time(t_file *lst)
{
	time_t		now;
	time_t		mtime;
	time_t		diff;
	char		*mtime_str;

	now = time(NULL);
	mtime = lst->ttime;
	mtime_str = ctime(&mtime);
	diff = now - mtime;
	ft_printf(" %.6s ", &(mtime_str[4]));
	if (diff < (-3600 * 24 * 30.5 * 6) || diff > (3600 * 24 * 30.5 * 6))
		ft_printf("%.4s", &(mtime_str[20]));
	else
		ft_printf("%.5s", &(mtime_str[11]));
}

static void			print_space(size_t n, size_t len)
{
	size_t			i;
	size_t			flen;
	char			s[NAME_MAX + 1];

	i = 0;
	flen = (len < n) ? n - (len - 1) : 1;
	ft_memset(s, ' ', NAME_MAX);
	if (flen < NAME_MAX)
		write(1, s, flen);
	else
	{
		while (i < flen)
		{
			ft_putchar(' ');
			i++;
		}
	}
}

static void			print_node(t_file *lst, t_data *data)
{
	ft_printf("%s ", lst->perm);
	print_space(data->space[1], ft_intlen((int)lst->st_nlink));
	ft_printf("%hu", lst->st_nlink);
	ft_printf(" %s ", getpwuid(lst->st_uid)->pw_name);
	print_space(data->space[2], ft_strlen(getpwuid(lst->st_uid)->pw_name));
	ft_printf("%s", getgrgid(lst->st_gid)->gr_name);
	print_space(data->space[3], ft_strlen(getgrgid(lst->st_gid)->gr_name));
	if (lst->perm[0] != 'c' && lst->perm[0] != 'b')
	{
		print_space(data->space[4], ft_intlen((int)lst->size));
		ft_printf("%lld", lst->size);
	}
	else
	{
		print_space(data->space[5], ft_intlen((int)major(lst->st_rdev)));
		ft_printf("%ld", major(lst->st_rdev));
		print_space(data->space[6], ft_intlen((int)minor(lst->st_rdev)));
		ft_printf("%ld", minor(lst->st_rdev));
	}
	print_time(lst);
}

void				display_list(bool tot, bool col, t_file *lst, t_data *data)
{
	t_file			*tmp;

	tmp = lst;
	ft_get_size(data, lst);
	if (tot)
		ft_printf("total %lld\n", data->space[0]);
	while (tmp)
	{
		if (tmp->valid)
		{
			print_node(tmp, data);
			display_name(col, tmp);
		}
		tmp = tmp->next;
	}
}
