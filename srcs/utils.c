/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 19:05:45 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/01 12:23:14 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void				display_name(bool col, t_file *lst)
{
	char			lnk[NAME_MAX + 1];

	if (col)
	{
		if (S_ISDIR(lst->mode))
			ft_printf(" {BLUE}%s{EOC}", lst->name);
		else if (S_IXUSR & lst->mode)
			ft_printf(" {RED}%s{EOC}", lst->name);
		else
			ft_printf(" %s", lst->name);
	}
	else
		ft_printf(" %s", lst->name);
	if (lst->perm[0] == 'l')
	{
		readlink(lst->full_path, lnk, NAME_MAX);
		ft_printf(" -> %s", lnk);
	}
	ft_putchar('\n');
}

char				ft_gettype(unsigned int mode)
{
	char			c;

	if (S_ISDIR(mode) == 1)
		c = 'd';
	else if (S_ISLNK(mode) == 1)
		c = 'l';
	else if (S_ISFIFO(mode) == 1)
		c = 'p';
	else if (S_ISBLK(mode) == 1)
		c = 'b';
	else if (S_ISCHR(mode) == 1)
		c = 'c';
	else if (S_ISSOCK(mode) == 1)
		c = 's';
	else
		c = '-';
	return (c);
}

bool				ft_fullpath(char *path, char *next, char **full_path)
{
	*full_path = ft_strjoin(path, "/");
	*full_path = ft_strjoin_free(*full_path, next, 1);
	if (ft_strlen(*full_path) > PATH_MAX)
	{
		errno = ENAMETOOLONG;
		ft_error((char *)path, false);
		return (false);
	}
	return (true);
}

void				ft_usage(char *s)
{
	ft_printf("{RED}Invalid command [ %s ]{RESET}\n"
			"Usage: ls [Option]... <Filename>\n"
			"	-a, do not ignore entries starting with .\n"
			"	-l, use a long listing format\n"
			"	-r, reverse order while sorting\n"
			"	-t, sort by modification of time, newest first\n"
			"	-R, list subdirectories recursively\n"
			"	-G, list with Colors\n"
			, s);
	exit(EXIT_FAILURE);
}

void				ft_error(char *s, bool ext)
{
	ft_putstr_fd("ls: ", 2);
	ft_putstr_fd(s, 2);
	ft_putstr_fd(": ", 2);
	ft_putstr_fd(strerror(errno), 2);
	ft_putendl_fd("", 2);
	if (ext == true)
		exit(EXIT_FAILURE);
}
