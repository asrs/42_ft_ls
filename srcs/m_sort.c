/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_sort.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/15 18:51:29 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/04 15:00:47 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_file			*sort_rev(t_file *lst)
{
	t_file *one;
	t_file *two;
	t_file *three;

	if (!lst)
		return (NULL);
	one = NULL;
	two = lst;
	three = (lst)->next;
	while (two)
	{
		three = two->next;
		two->next = one;
		one = two;
		two = three;
	}
	return (one);
}

static t_file			*sort_time(t_file *lst)
{
	if (!lst)
		return (NULL);
	if (lst->next && (lst->ttime < lst->next->ttime))
		lst = node_swap(lst, lst->next);
	else if (lst->next && lst->ttime == lst->next->ttime)
		if (lst->next && (lst->ntime < lst->next->ntime))
			lst = node_swap(lst, lst->next);
	lst->next = sort_time(lst->next);
	if (lst->next && (lst->ttime < lst->next->ttime))
	{
		lst = node_swap(lst, lst->next);
		lst->next = sort_time(lst->next);
	}
	else if (lst->next && lst->ttime == lst->next->ttime)
	{
		if (lst->next && (lst->ntime < lst->next->ntime))
		{
			lst = node_swap(lst, lst->next);
			lst->next = sort_time(lst->next);
		}
	}
	return (lst);
}

static t_file			*sort_ascii(t_file *lst)
{
	if (!lst)
		return (NULL);
	if (lst->next && ft_strcmp(lst->name, lst->next->name) > 0)
		lst = node_swap(lst, lst->next);
	lst->next = sort_ascii(lst->next);
	if (lst->next && ft_strcmp(lst->name, lst->next->name) > 0)
	{
		lst = node_swap(lst, lst->next);
		lst->next = sort_ascii(lst->next);
	}
	return (lst);
}

void					m_sort(t_file **lst, t_data *data)
{
	*lst = sort_ascii(*lst);
	if (data->flags & LS_T)
		*lst = sort_time(*lst);
	if (data->flags & LS_R)
		*lst = sort_rev(*lst);
}
