/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_colsize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 14:13:39 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 15:45:09 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static size_t		max(size_t a, size_t b)
{
	return ((a > b) ? a : b);
}

static bool			check_valid(t_file *lst)
{
	struct passwd	*passwd;
	struct group	*group;

	passwd = getpwuid(lst->st_uid);
	group = getgrgid(lst->st_gid);
	if (!passwd || !group)
		return (false);
	return (true);
}

static void			call_size(t_data *data, t_file *lst)
{
	size_t			i;
	size_t			len;

	len = 0;
	if (!check_valid(lst))
	{
		lst->valid = false;
		return ;
	}
	i = ft_intlen((int)lst->st_nlink);
	data->space[1] = max(i, (size_t)data->space[1]);
	i = ft_strlen(getpwuid(lst->st_uid)->pw_name);
	data->space[2] = max(i, data->space[2]);
	i = ft_strlen(getgrgid(lst->st_gid)->gr_name);
	data->space[3] = max(i, data->space[3]);
	if (!S_ISCHR(lst->mode))
		len = ft_intlen((int)lst->size);
	else
	{
		data->space[5] = ft_intlen((int)major(lst->st_rdev));
		i = ft_intlen((int)minor(lst->st_rdev));
		data->space[6] = max(i, data->space[6]);
		len = data->space[5] + data->space[6] + 2;
	}
	data->space[4] = max(len, data->space[4]);
}

void				ft_get_size(t_data *data, t_file *lst)
{
	data->space[0] = 0;
	data->space[1] = 0;
	data->space[3] = 0;
	data->space[2] = 0;
	data->space[4] = 0;
	data->space[5] = 3;
	data->space[6] = 3;
	while (lst)
	{
		data->space[0] += (size_t)lst->st_blocks;
		call_size(data, lst);
		lst = lst->next;
	}
	data->space[5] = max((data->space[4] - data->space[6]), data->space[5]);
}
