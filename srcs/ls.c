/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 17:42:01 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/03 21:15:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int					main(int ac, char **av)
{
	int				i;
	t_data			data;

	ft_bzero(&data, sizeof(t_data));
	i = m_parse(ac, av, &data);
	if (ac <= i)
	{
		ft_strcpy(data.path, ".");
		if ((data.flags & LS_RC))
			m_getdir(true, true, data.path, &data);
		else
			m_getdir(false, false, data.path, &data);
	}
	else
		m_launcher(i, ac, av, &data);
	return (0);
}
