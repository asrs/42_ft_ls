/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/15 18:17:16 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/01 16:04:59 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void			del_file(t_file *lst)
{
	t_file			*tmp;

	tmp = lst;
	while (tmp->next->next)
		tmp = tmp->next;
	if (tmp->next)
	{
		(tmp->next->name) ? ft_strdel(&tmp->next->name) : 0;
		(tmp->next->perm) ? ft_strdel(&tmp->next->perm) : 0;
		free(tmp->next);
		tmp->next = NULL;
	}
}

void				mc_clean(t_file *head)
{
	if (!head)
		return ;
	while (head->next)
		del_file(head);
	if (head->next)
	{
		(head->next->name) ? ft_strdel(&head->next->name) : 0;
		(head->next->perm) ? ft_strdel(&head->next->perm) : 0;
		free(head->next);
		head->next = NULL;
	}
	if (head)
	{
		(head->name) ? ft_strdel(&head->name) : 0;
		(head->perm) ? ft_strdel(&head->perm) : 0;
		free(head);
		head = NULL;
	}
}

t_file				*node_swap(t_file *p1, t_file *p2)
{
	p1->next = p2->next;
	p2->next = p1;
	return (p2);
}
