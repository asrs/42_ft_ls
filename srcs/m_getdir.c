/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_getdir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 15:32:14 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/03 19:58:04 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			get_rec(t_file **current, t_data *data)
{
	t_file			*tmp;

	tmp = *current;
	if (!(*current))
		return (-1);
	while (tmp->next)
	{
		if (tmp->act == true)
		{
			tmp->act = false;
			m_getdir(true, true, tmp->full_path, data);
		}
		tmp = tmp->next;
	}
	if (tmp->act == true)
	{
		tmp->act = false;
		m_getdir(true, true, tmp->full_path, data);
	}
	return (0);
}

int					m_getdir(bool rec, bool title, char *path, t_data *data)
{
	DIR				*folder;
	t_file			*lst;
	struct dirent	*current;

	data->rd_dir++;
	lst = NULL;
	if (!(folder = opendir(path)))
	{
		ft_error(data->path, false);
		return (-1);
	}
	while ((current = readdir(folder)))
		if (current->d_name[0] != '.' || (data->flags & LS_A))
			m_file(path, current->d_name, (int)data->flags, &lst);
	if (folder)
		closedir(folder);
	m_sort(&lst, data);
	m_display(true, title, lst, data);
	if (rec == true)
		get_rec(&lst, data);
	mc_clean(lst);
	return (0);
}
