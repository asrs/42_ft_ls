/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrjoin_free.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:51 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 22:38:33 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

wchar_t			*ft_wstrjoin_free(const wchar_t *s1, const wchar_t *s2,
		int index)
{
	wchar_t		*dest;

	if (!(s1 && s2))
		return (NULL);
	else if (!(dest = ft_wstrnew(ft_wstrlen(s1) + ft_wstrlen(s2))))
		return (NULL);
	if (s1)
		dest = ft_wstrcat(dest, s1);
	if (s2)
		dest = ft_wstrcat(dest, s2);
	if (index == 1)
		free((void *)s1);
	else if (index == 2)
		free((void *)s2);
	else if (index == 3)
	{
		free((void *)s1);
		free((void *)s2);
	}
	return (dest);
}
