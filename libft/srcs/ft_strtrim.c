/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:48 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:48 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_ischar(char s, char c)
{
	return (s == c);
}

char				*ft_strtrim(char const *s, char c)
{
	int				i;
	int				j;
	size_t			len;

	if (!s)
		return (NULL);
	len = ft_strlen(s) - 1;
	i = 0;
	j = 0;
	while (ft_ischar(s[i], c))
		i++;
	if (*(s + i) == '\0')
		return (ft_strnew(0));
	while (ft_ischar(s[(int)len - j], c))
		j++;
	len = ft_strlen(s) - (size_t)(i + j);
	return (ft_strsub(s, (unsigned int)i, len));
}
