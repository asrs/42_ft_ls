/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_int.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 17:39:28 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:51:41 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		get_value(char *s, t_dna *dna)
{
	FLAG.value.tint = va_arg(dna->va, long long);
	if (ft_strnchr(s, 'l') == 2)
		FLAG.s_flag = ft_itoa_base((long long)FLAG.value.tint, 10);
	else if (ft_strnchr(s, 'l') == 1 || ft_strnchr(s, 'D') == 1)
		FLAG.s_flag = ft_itoa_base((long)FLAG.value.tint, 10);
	else if (ft_strnchr(s, 'h') == 2)
		FLAG.s_flag = ft_itoa_base((char)FLAG.value.tint, 10);
	else if (ft_strnchr(s, 'h') == 1)
		FLAG.s_flag = ft_itoa_base((short)FLAG.value.tint, 10);
	else if (ft_strnchr(s, 'j') == 1)
		FLAG.s_flag = ft_itoa_base((intmax_t)FLAG.value.tint, 10);
	else if (ft_strnchr(s, 'z') == 1)
		FLAG.s_flag = ft_itoa_base((ssize_t)FLAG.value.tint, 10);
	else
		FLAG.s_flag = ft_itoa_base((int)FLAG.value.tint, 10);
}

static void		cancel(t_dna *dna)
{
	if ((FLAG.modifier >> 0 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 0);
	if ((FLAG.modifier >> 1 & 1) == 1 && (FLAG.modifier >> 2 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 1);
	else if ((FLAG.modifier >> 1 & 1) == 1 && (FLAG.modifier >> 5 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 1);
	else if ((FLAG.modifier >> 3 & 1) == 1 && (FLAG.modifier >> 4 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 3);
}

static void		process(t_dna *dna)
{
	if (FLAG.s_flag[0] == '-')
	{
		FLAG.s_sign = set_sign(1);
		FLAG.s_flag = ft_strsub_free(FLAG.s_flag, 1, ft_strlen(FLAG.s_flag));
	}
	if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding += ((FLAG.modifier
					>> 3 & 1) == 1) ? (FLAG.v_size) : FLAG.v_size;
		FLAG.v_size = 0;
	}
	if ((FLAG.modifier >> 3 & 1) == 1 && FLAG.s_sign == NULL)
		FLAG.s_sign = set_sign(2);
	else if ((FLAG.modifier >> 4 & 1) == 1 && FLAG.s_sign == NULL)
		FLAG.s_sign = set_sign(0);
	if (ft_strequ("0", FLAG.s_flag) && ((FLAG.modifier >> 5 & 1) == 1)
			&& FLAG.v_padding == 0)
	{
		ft_strdel(&FLAG.s_flag);
		FLAG.s_flag = ft_strdup("");
	}
}

bool			ex_int(char *s)
{
	t_dna		*dna;

	dna = call();
	get_value(s, dna);
	cancel(dna);
	process(dna);
	FLAG.len = (long)ft_strlen(FLAG.s_flag);
	return (true);
}
